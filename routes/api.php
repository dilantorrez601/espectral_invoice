<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\UnitController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\NoteController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('login', [AuthController::class, 'login']);

Route::group(['prefix' => 'auth','middleware' => 'jwt.auth'], function ($router) {

    Route::post('logout', [AuthController::class, 'logout']);
    Route::post('refresh',[AuthController::class, 'refresh']);

    // Route::get('/', 'HomeController@index')->name('home');
    Route::resource('client',ClientController::class);
    Route::resource('product',ProductController::class);
    Route::resource('category',CategoryController::class);
    Route::resource('unit',UnitController::class);
    Route::resource('invoice',InvoiceController::class);
    Route::resource('note',NoteController::class);

    //helper for multiselect
    Route::get('categories', [CategoryController::class, 'list']);
    Route::get('units', [UnitController::class, 'list']);
    Route::get('clients', [ClientController::class, 'list']);
    Route::get('products', [ProductController::class, 'list']);

    //for print pdf
    Route::post('invoice_pdf', [ReportController::class, 'invoicePdf']);


});
