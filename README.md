<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>


## Espectral Invoice


## Install
`cp .env.example .env `

`composer install`
   
`npm Install`

`php artisan key:generate ` 

`php artisan jwt:secret  `

## .env 

add url proyect into .env file

example "http://espectral_invoice.test" 

APP_URL=http://espectral_invoice.test

then make run: 

`npm run dev`

## Migrate Data Base

To migrate proyect need run this comand:

`php artisan migrate --seed`


## Utils
for send a message with vuex use this:

this.$store.dispatch('template/showMessage',{message:'Se registro correctamente el Gasto ',color:'success'});

where color options is 'success', 'info', 'error'

show/close loading dialog, add this line in your method
	this.$store.dispatch('template/showLoading');
	this.$store.dispatch('template/closeLoading');

## Contributing

Leandro David Torrez Salinas

## License 

This project is licensed under the MIT License

