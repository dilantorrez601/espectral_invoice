<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class TriggersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::unprepared("
        /*Trigger branches*/

        CREATE OR REPLACE FUNCTION before_insert_branch()
          RETURNS trigger AS
        $$
        begin
            NEW.public_id = (SELECT COALESCE (MAX(public_id),0) + 1 FROM public.branches WHERE account_id = NEW.account_id );
            RETURN NEW;
        end
        $$
        LANGUAGE 'plpgsql';

        create trigger branch_public_id
        before insert
        on public.branches
        for each row
        execute procedure before_insert_branch();

         /* Triger Category XD*/
        CREATE OR REPLACE FUNCTION before_insert_category()
          RETURNS trigger AS
        $$
        begin
            NEW.public_id = (SELECT COALESCE (MAX(public_id),0) + 1 FROM public.categories WHERE account_id = NEW.account_id );
            RETURN NEW;
        end
        $$
        LANGUAGE 'plpgsql';

        create trigger category_public_id
        before insert
        on public.categories
        for each row
        execute procedure before_insert_category();

        /*Trigger clients*/

        CREATE OR REPLACE FUNCTION before_insert_client()
          RETURNS trigger AS
        $$
        begin
            NEW.public_id = (SELECT COALESCE (MAX(public_id),0) + 1 FROM public.clients WHERE account_id = NEW.account_id );
            RETURN NEW;
        end
        $$
        LANGUAGE 'plpgsql';

        create trigger client_public_id
        before insert
        on public.clients
        for each row
        execute procedure before_insert_client();

        /*Trigger contacts*/

        CREATE OR REPLACE FUNCTION before_insert_contact()
          RETURNS trigger AS
        $$
        begin
            NEW.public_id = (SELECT COALESCE (MAX(public_id),0) + 1 FROM public.contacts WHERE account_id = NEW.account_id );
            RETURN NEW;
        end
        $$
        LANGUAGE 'plpgsql';

        create trigger branch_public_id
        before insert
        on public.contacts
        for each row
        execute procedure before_insert_contact();

        /*Trigger document types*/

        CREATE OR REPLACE FUNCTION before_insert_document_type()
          RETURNS trigger AS
        $$
        begin
            NEW.public_id = (SELECT COALESCE (MAX(public_id),0) + 1 FROM public.document_types WHERE account_id = NEW.account_id );
            RETURN NEW;
        end
        $$
        LANGUAGE 'plpgsql';

        create trigger document_type_public_id
        before insert
        on public.document_types
        for each row
        execute procedure before_insert_document_type();

        /*Trigger invoice_items*/

        CREATE OR REPLACE FUNCTION before_insert_invoice_item()
          RETURNS trigger AS
        $$
        begin
            NEW.public_id = (SELECT COALESCE (MAX(public_id),0) + 1 FROM public.invoice_items WHERE account_id = NEW.account_id );
            RETURN NEW;
        end
        $$
        LANGUAGE 'plpgsql';

        create trigger invoice_item_public_id
        before insert
        on public.invoice_items
        for each row
        execute procedure before_insert_invoice_item();

        /*Trigger invoices*/

        CREATE OR REPLACE FUNCTION before_insert_invoice()
          RETURNS trigger AS
        $$
        begin
            NEW.public_id = (SELECT COALESCE (MAX(public_id),0) + 1 FROM public.invoices  WHERE account_id = NEW.account_id );
            RETURN NEW;
        end
        $$
        LANGUAGE 'plpgsql';

        create trigger invoice_public_id
        before insert
        on public.invoices
        for each row
        execute procedure before_insert_invoice();

        /*Trigger payments*/

        CREATE OR REPLACE FUNCTION before_insert_payment()
          RETURNS trigger AS
        $$
        begin
            NEW.public_id = (SELECT COALESCE (MAX(public_id),0) + 1 FROM public.payments WHERE account_id = NEW.account_id );
            RETURN NEW;
        end
        $$
        LANGUAGE 'plpgsql';

        create trigger payment_public_id
        before insert
        on public.payments
        for each row
        execute procedure before_insert_payment();

        /*Trigger products*/

        CREATE OR REPLACE FUNCTION before_insert_product()
          RETURNS trigger AS
        $$
        begin
            NEW.public_id = (SELECT COALESCE (MAX(public_id),0) + 1 FROM public.products WHERE account_id = NEW.account_id );
            RETURN NEW;
        end
        $$
        LANGUAGE 'plpgsql';

        create trigger product_public_id
        before insert
        on public.products
        for each row
        execute procedure before_insert_product();

        /*Trigger units*/

        CREATE OR REPLACE FUNCTION before_insert_unit()
          RETURNS trigger AS
        $$
        begin
            NEW.public_id = (SELECT COALESCE (MAX(public_id),0) + 1 FROM public.units WHERE account_id = NEW.account_id );
            RETURN NEW;
        end
        $$
        LANGUAGE 'plpgsql';

        create trigger unit_public_id
        before insert
        on public.units
        for each row
        execute procedure before_insert_unit();

        /*Trigger users*/

        CREATE OR REPLACE FUNCTION before_insert_user()
          RETURNS trigger AS
        $$
        begin
            NEW.public_id = (SELECT COALESCE (MAX(public_id),0) + 1 FROM public.users WHERE account_id = NEW.account_id );
            RETURN NEW;
        end
        $$
        LANGUAGE 'plpgsql';

        create trigger user_public_id
        before insert
        on public.users
        for each row
        execute procedure before_insert_user();

        ");
    }
}
