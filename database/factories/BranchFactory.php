<?php

namespace Database\Factories;

use App\Models\Branch;
use Illuminate\Database\Eloquent\Factories\Factory;

class BranchFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Branch::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'account_id' => 1,
            'user_id' => 1,
            'name' => $this->faker->name,
            'address1' => $this->faker->address,
            'address2' => $this->faker->address,
            'city' => $this->faker->country,
            'work_phone' => $this->faker->e164PhoneNumber,
            'sfc' => $this->faker->numberBetween($min=1,$max=10),
            'number_branch' => $this->faker->numberBetween($min=1,$max=10),
            'key_dosage' => $this->faker->regexify('[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}'),
            'number_autho' => $this->faker->numberBetween($min=10000000,$max=99999999),
            'economic_activity' => $this->faker->sentence($nbWords=6, $variableNbWords = true),
            'law' => $this->faker->sentence($nbWords=6, $variableNbWords = true),
            'deadline' => '2021-01-01',
        ];
    }
}
