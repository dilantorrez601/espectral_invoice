<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoiceItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_items', function (Blueprint $table) {
            $table->id();
            $table->foreignId('account_id');
            $table->foreignId('branch_id');
            $table->foreignId('invoice_id');
            $table->foreignId('product_id');
            $table->foreignId('user_id');

            $table->boolean('is_product');
            $table->string('product_key');
            $table->text('notes');
            $table->decimal('cost', 13, 2);
            $table->decimal('quantity', 13, 2)->nullable();
            $table->float('discount');
            $table->string('unit'); //guardamos el nombre solamente XD


            $table->unsignedInteger('public_id')->index();
            $table->unique( array('account_id','public_id'));
            $table->foreign('account_id')->references('id')->on('accounts');
            $table->foreign('invoice_id')->references('id')->on('invoices');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('product_id')->references('id')->on('products');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_items');
    }
}
