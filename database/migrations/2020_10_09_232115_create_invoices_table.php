<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->id();
            $table->foreignId('account_id');
            $table->foreignId('branch_id');
            $table->foreignId('client_id');
            $table->foreignId('user_id');
            $table->foreignId('invoice_status_id')->default(1);
            $table->foreignId('frecuency_id')->nullable();
            $table->foreignId('document_type_id');

            $table->string('invoice_number');
            $table->date('invoice_date')->nullable();
            $table->date('due_date')->nullable();
            $table->float('discount')->nullable();
            $table->string('po_number')->nullable();

            $table->text('terms')->nullable();
            $table->text('public_notes')->nullable();
            $table->text('note')->nullable();
            /* //datos solo de recurrencia
            $table->boolean('is_recurring');
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->timestamp('last_sent_date')->nullable();
            */
            $table->string('account_name');
            $table->string('account_nit');
            $table->string('account_uniper')->nullable();
            $table->string('sfc')->nullable();

            $table->string('branch_name');
            $table->string('address1');
            $table->string('address2')->nullable();
            $table->string('phone')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('number_autho')->nullable();
            $table->date('deadline')->nullable();
            $table->string('key_dosage')->nullable();
            $table->boolean('type_third')->default(false);

            $table->string('client_nit')->nullable();
            $table->string('client_name');

            $table->string('economic_activity')->nullable();

            $table->string('law')->nullable();

            $table->string('control_code')->nullable();

            $table->string('qr')->nullable();

            $table->decimal('debito_fiscal')->nullable();
            $table->decimal('importe_neto')->nullable();
            $table->decimal('importe_total')->nullable();
            $table->decimal('descuento_total')->nullable();
            $table->decimal('balance')->nullable();

            $table->unsignedInteger('public_id')->index();
            $table->unique( array('account_id','public_id'));
            $table->foreign('account_id')->references('id')->on('accounts');
            $table->foreign('client_id')->references('id')->on('clients');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('invoice_status_id')->references('id')->on('invoice_statuses');
            $table->foreign('frecuency_id')->references('id')->on('frecuencies');
            $table->foreign('document_type_id')->references('id')->on('document_types');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
