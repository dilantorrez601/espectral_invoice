<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activities', function (Blueprint $table) {
            $table->id();
            $table->foreignId('account_id');
            $table->foreignId('client_id');
            $table->foreignId('user_id');
            $table->foreignId('contact_id')->nullable();
            $table->foreignId('payment_id')->nullable();
            $table->foreignId('invoice_id')->nullable();
            $table->text('message')->nullable();
            $table->text('json_backup')->nullable();
            $table->integer('activity_type_id');  //constante de la clase activity
            $table->decimal('adjustment', 13, 2)->nullable();
            $table->decimal('balance', 13, 2)->nullable();

            $table->foreign('account_id')->references('id')->on('accounts');
            $table->foreign('invoice_id')->references('id')->on('invoices');
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activities');
    }
}
