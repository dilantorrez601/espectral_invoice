<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBranchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branches', function (Blueprint $table) {
            $table->id();
            $table->foreignId('account_id');
            $table->foreignId('user_id');
            $table->string('name');
            $table->string('address1');
            $table->string('address2')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('work_phone')->nullable();
            $table->string('sfc')->nullable();
            $table->integer('number_branch')->nullable();//adicionado para el numero de de sucursal
            $table->string('number_process')->nullable();
            $table->string('number_autho')->nullable();
            $table->date('deadline')->nullable();
            $table->string('key_dosage')->nullable();
            $table->string('economic_activity')->nullable();
            $table->string('law')->nullable();
            $table->boolean('type_third')->default(false);
            $table->integer('invoice_number_counter')->default(0); //metodo de control de facturas contador de facturas
            // $table->text('quote_number_prefix')->nullable();  //prefijos
            $table->integer('quote_number_counter')->default(0)->nullable(); //cotizaciones numeracion

            $table->unsignedInteger('public_id')->index();
            $table->unique( array('account_id','public_id'));
            $table->foreign('account_id')->references('id')->on('accounts');
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branches');
    }
}
