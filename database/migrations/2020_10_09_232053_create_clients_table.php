<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table)
        {
            $table->id();
            $table->foreignId('account_id');
            $table->foreignId('user_id');
            $table->string('name');
            $table->string('business_name')->nullable();
            $table->string('nit');

            $table->string('address1')->nullable();
            $table->string('address2')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();

            $table->string('work_phone')->nullable();
            $table->text('private_notes')->nullable();

            $table->decimal('balance', 13, 2)->nullable();
            //$table->decimal('paid_to_date', 13, 2)->nullable(); //en caso de facturas recurrentes

            $table->string('custom_value1')->nullable();
            $table->string('custom_value2')->nullable();
            $table->string('custom_value3')->nullable();
            $table->string('custom_value4')->nullable();
            $table->string('custom_value5')->nullable();
            $table->string('custom_value6')->nullable();
            $table->string('custom_value7')->nullable();
            $table->string('custom_value8')->nullable();
            $table->string('custom_value9')->nullable();
            $table->string('custom_value10')->nullable();
            $table->string('custom_value11')->nullable();
            $table->string('custom_value12')->nullable();

            $table->unsignedInteger('public_id')->index();
            $table->unique( array('account_id','public_id'));
            $table->foreign('account_id')->references('id')->on('accounts');
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
