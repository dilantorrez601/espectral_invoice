<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->id();
            $table->foreignId('currency_id')->nullable();
            $table->foreignId('time_zone_id')->nullable();
            $table->foreignId('date_format_id')->nullable();
            $table->foreignId('date_time_format_id')->nullable();

            $table->string('account_key')->nullable();  //llave para la creacion de cuenta XD
            $table->string('domain')->unique();
            $table->string('nit')->unique();
            $table->string('name')->unique();
            // $table->string('address')->nullable(); //estos datos estan en la sucursal caza matriz

            $table->boolean('confirmed')->default(false);
            $table->integer('credit_counter')->default(0); //contador de cuantas facturas se le deja facturar
            $table->date('billing_deadline')->null();  //fecha de vencimiento

            $table->boolean('is_uniper')->default(false); //si es unipersonal
            $table->string('uniper')->nullable();

            $table->string('custom_client_label1')->nullable();
            $table->string('custom_client_label2')->nullable();
            $table->string('custom_client_label3')->nullable();
            $table->string('custom_client_label4')->nullable();
            $table->string('custom_client_label5')->nullable();
            $table->string('custom_client_label6')->nullable();
            $table->string('custom_client_label7')->nullable();
            $table->string('custom_client_label8')->nullable();
            $table->string('custom_client_label9')->nullable();
            $table->string('custom_client_label10')->nullable();
            $table->string('custom_client_label11')->nullable();
            $table->string('custom_client_label12')->nullable();

            $table->foreign('currency_id')->references('id')->on('currencies');
            $table->foreign('time_zone_id')->references('id')->on('time_zones');
            $table->foreign('date_format_id')->references('id')->on('date_formats');
            $table->foreign('date_time_format_id')->references('id')->on('date_time_formats');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts');
    }
}
