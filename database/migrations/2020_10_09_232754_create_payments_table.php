<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->id();
            $table->foreignId('account_id');
            $table->foreignId('invoice_id');
            $table->foreignId('client_id');
            $table->foreignId('contact_id')->nullable();
            $table->foreignId('payment_type_id');
            $table->foreignId('user_id');

            $table->decimal('amount', 13, 2);
            $table->date('payment_date')->nullable();
            $table->string('transaction_reference')->nullable();
            //$table->string('payer_id')->nullable();  //no recuerdo el motivo me parece que es el id de la transaccion en linea o algo asi
            $table->unsignedInteger('public_id')->index();
            $table->unique( array('account_id','public_id'));
            $table->foreign('account_id')->references('id')->on('accounts');
            $table->foreign('invoice_id')->references('id')->on('invoices');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('payment_type_id')->references('id')->on('payment_types');
            $table->foreign('client_id')->references('id')->on('clients');
            $table->foreign('contact_id')->references('id')->on('contacts');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
