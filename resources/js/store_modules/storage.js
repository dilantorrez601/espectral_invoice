export const storage = {
    namespaced: true,
    state:{
        drawer:null,
        show_message:false,//para el dialog
        message:null,//contenido del mensaje
        path:null,//direccion
        // color:'success',
        // dialog_confirm:false,// dialog de mesaje de confirmacion
        // show_loading:false,
        // title:'',
        // content:'',
        // response:false,
    },
    mutations:{


        setShowMessage(state,{message,show_message,path}){
            state.message = message;
            state.show_message = show_message;
            state.path = path;

        },
        setCloseMessage(state){
            state.show_message =false;
        },



    },
    actions:{
        showMessage({commit},{message,path}){
            commit('setShowMessage', {message:message,show_message:true,path:path});
        },
        closeMessage({commit}){
            commit('setCloseMessage');
        }

    }
};
