window._ = require('lodash');

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}



window.iziToast = require('izitoast');//notificaciones
window.moment = require('moment');

// window.PdfPrinter = require('pdfmake');
window.url_base = window.location.origin;

import Vue from 'vue';
import App from './components/App.vue';
import VueRouter from 'vue-router';
import {storage} from './store_modules/storage';
import {autentication} from './store_modules/autentication';
import {routes} from './routes';
import Vuex from 'vuex';
import Multiselect from 'vue-multiselect'
import Tooltip, { vueDirectiveTooltip } from 'vue-directive-tooltip';
import VueDatePicker from '@mathieustan/vue-datepicker';
// import VueCtkDateTimePicker from 'vue-ctk-date-time-picker';//buenos stylos pero es pesado para el proyecto
// import VueFlatPickr from 'vue-flatpickr-component';

// Vue.component('FlatPickr', VueFlatPickr);
// Vue.component('VueCtkDateTimePicker', VueCtkDateTimePicker);
Vue.component('multiselect', Multiselect)

Vue.use(VueDatePicker, {
    lang: 'es'
  });
Vue.use(VueRouter)
Vue.use(Vuex);
Vue.use(Tooltip);

Vue.prototype.$http = axios;
const tokenJWT = localStorage.getItem('token')
if (tokenJWT) {
  axios.defaults.headers.common['Authorization'] = 'Bearer ' + tokenJWT;
}
const store = new Vuex.Store({
    modules:{
        template: storage,
        auth: autentication,
        dconfirm: confirm,
    }
});


const router = new VueRouter({
    mode: 'history',
    routes: routes
});

router.beforeEach((to, from, next) => {
    if(to.matched.some(record => record.meta.requiresAuth)) {//add is login aqui
      if (store.getters['auth/isLoggedIn']) {
        next()
        return
      }
      next('/login')
    } else {
      next()
    }
});


const app = new Vue({
    el: '#app',
    components: { App },
    router,
    store,
});


