let env = process.env.MIX_SERVER_MODE;
console.log('ENV',env)
export const mixin =
{
	data:()=>({
		util:null,
        mode_server: env.toString()=="true"?true:false,
        url_base: url_base
	}),
	mounted(){
	  console.log('loading mixins ',this.mode_server);

	},
	methods: {
		formatDate(value)
		{
            moment.locale('es');
			return moment(value).format('ll');;
		},
        show_message(type_message,message)
        {
            switch(type_message){
                case 'info':
                    iziToast.info({
                        title: '',
                        message: message,
                        position: 'bottomRight' // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter
                    })
                    break
                case 'success':
                    iziToast.success({
                        title: '',
                        message: message,
                        position: 'bottomRight' // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter
                    })
                    break;
                case 'error':
                    iziToast.error({
                        title: '',
                        message: message,
                        position: 'bottomRight' // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter
                    })
                    break;
            }
        },
        formatNumber(number)
        {
            // In en-US, logs '100,000.00'
            // In de-DE, logs '100.000,00'
            // In hi-IN, logs '1,00,000.00'
            return parseFloat(number).toLocaleString(
            'en-US', // leave undefined to use the browser's locale,
                        // or use a string like 'en-US' to override it.
            { minimumFractionDigits: 2 }
            );
        }
	}
  }
