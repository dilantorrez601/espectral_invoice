
import Home from './components/Home';
import Login from './components/auth/Login';
import Client from './components/client/Index';
import ClientEdit from './components/client/Edit';
import Product from './components/product/Index';
import ProductEdit from './components/product/Edit';
import Category from './components/category/Index';
import CategoryEdit from './components/category/Edit';
import Unit from './components/unit/Index';
import UnitEdit from './components/unit/Edit';
import Invoice from './components/invoice/Index';
import Payment from './components/payment/Index';
import Note from './components/note/Index';
import NoteCreate from './components/note/Create';
  export const routes = [
        {
            path: '/',
            name: 'Inicio',
            components:{
                default:Home,
            },
            meta: {
                requiresAuth: true,
                breadcrumbs:[]
            },
        },
        {
            path: '/login',
            name: 'Inicar Sesion',
            component: Login,
        },
        {
            path: '/client',
            name: 'Clientes',
            components:{
                default:Client,
            },
            meta: {
                requiresAuth: true,
                breadcrumbs:[]
            },
        },
        {
            path: '/client/:id/edit',
            name: 'Editar',
            components:{
                default:ClientEdit,
            },
            meta: {
                requiresAuth: true,
                breadcrumbs:[{path:'/client',name:'Clientes'}]
            },

        },
        {
            path: '/product',
            name: 'Productos',
            components:{
                default:Product,
            },
            meta: {
                requiresAuth: true,
                breadcrumbs:[]
            },
        },
        {
            path: '/product/:id/edit',
            name: 'Editar',
            components:{
                default:ProductEdit,
            },
            meta: {
                requiresAuth: true,
                breadcrumbs:[{path:'/product',name:'Productos'}]
            },
        },

        {
            path: '/product/category',
            name: 'Categorias',
            components:{
                default:Category,
            },
            meta: {
                requiresAuth: true,
                breadcrumbs:[{path:'/product',name:'Productos'}]
            },
        },

        {
            path: '/product/unit',
            name: 'Unidades',
            components:{
                default:Unit,
            },
            meta: {
                requiresAuth: true,
                breadcrumbs:[{path:'/product',name:'Productos'}]
            },
        },

        {
            path: '/invoice',
            name: 'Facturas',
            components:{
                default:Invoice,
            },
            meta: {
                requiresAuth: true,
                breadcrumbs:[]
            },
        },
        {
            path: '/note',
            name: 'Notas',
            components:{
                default:Note,
            },
            meta: {
                requiresAuth: true,
                breadcrumbs:[]
            },
        },
        {
            path: '/note/create',
            name: 'Crear',
            components:{
                default:NoteCreate,
            },
            meta: {
                requiresAuth: true,
                breadcrumbs:[{path:'/note',name:'Notas'}]
            },
        },
        {
            path: '/payment',
            name: 'Pagos',
            components:{
                default:Payment,
            },
            meta: {
                requiresAuth: true,
                breadcrumbs:[]
            },
        },

    ];
