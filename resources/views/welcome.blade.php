<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Base Template</title>

        {{-- <link rel="stylesheet" href="{!! asset('css/loader.css') !!}" /> --}}
        <link rel="stylesheet" href="{!! asset('css/app.css') !!}" />
        <script src="{{ asset('js/app.js') }}" defer></script>

        <link rel="stylesheet" href="{!! asset('css/debug.css') !!}" />

    </head>
    <body >
        <section class="section">
            <div class="container">
               <h1 class="title">
                   hello bulma
               </h1>
               <p class="subitle">
                   mi primer website con <strong> Bulma</strong>
               </p>
            </div>

        </section>

    </body>
</html>
