<!DOCTYPE html>
<html lang="es">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>{{ $title??'' }}</title>
    {{-- <link rel="stylesheet" href="{{ public_path('css/materialicons.css') }}" media="all" /> --}}
    <link rel="stylesheet" href="{{ public_path('css/wkhtml.css') }}" media="all" />
</head>

<body style="border:none">
    <div style=" padding: 15px;">

        <div class="page-break" >
            <table class="w-100 ">
                <tr>
                    <th class="w-15 text-left no-padding no-margins align-middle">
                        {{-- <img src="{{ public_path('images/logo.png') }}" style=" width: 148px;"> --}}
                        <img src="{{$document_type->logo??''}}" style=" width: 148px;">
                    </th>
                    <th class="w-50 align-center text-left">

                        <p  >
                            <div class="font-thin text-xxxs uppercase no-paddings">
                               <strong class="font-semibold text-xxs uppercase leading-tight">{{ $branch->name ?? 'ARXAYIRI' }}</strong>   <br>
                                {{ $branch->address1 ?? 'Plaza Murillo 1234' }} <br>
                                {{ $branch->address2 ?? 'Plaza Murillo 1234' }} <br>
                                Telefono {{ $branch->work_phone ?? 'Plaza Murillo 1234' }} <br>
                                {{ $branch->city ?? 'La Paz' }} - {{$branch->state ?? 'bolivia' }}
                            </div>
                        </p>

                        <div class="watermark">
                            Vista Preliminar
                        </div>
                    </th>
                    <th class="w-20 no-padding  align-center">

                        <table class="table-code align-top no-padding no-margins">
                            <tbody>
                                <tr>
                                    <td class="text-center bg-grey-darker text-xxs text-white">Fecha de Emisión</td>
                                    <td class="text-xs">{{ $form->date??'' }}</td>
                                </tr>
                                <tr>
                                    <td class="text-center bg-grey-darker text-xxs text-white">Usuario</td>
                                    <td class="text-xs">{!! explode("@", $user->username)[0] ??'' !!}</td>
                                </tr>

                                <tr>
                                    <td class="text-center bg-grey-darker text-xxs text-white">Nota #</td>
                                    <td class="text-xs">{!! $form->invoice_number??'' !!}</td>
                                </tr>

                            </tbody>
                        </table>

                    </th>
                </tr>
                <tr class="no-border">
                    <td colspan="3" class="no-border" style="border-bottom: 1px solid #22292f;"></td>
                </tr>
                <tr>
                    <td colspan="3" class="font-bold text-center text-xl uppercase">
                        {{ $title??'' }}
                        @if (isset($subtitle))
                        <br><span class="font-medium">{{ $subtitle ?? '' }}</span>
                        @endif
                    </td>
                </tr>

            </table>
            <div class="block">
                <table class="table-info w-100">
                    <tbody>
                        <tr class="font-medium  ">
                            <td class=" text-white text-sm bg-grey-darker w-20 px-3 py text-center">
                                Cliente
                            </td>
                            <td class=" px-10">
                                {{ $client->name??''}}
                            </td>
                        </tr>
                    </tbody>

                </table>
            </div>


            <br>
            <div class="block">
                <table class="table-info w-100">

                    <thead class="bg-grey-darker">

                        <tr class="font-medium text-white text-sm">
                            <td class="w-20 px-3 py text-center text-xxs">
                                CODIGO
                            </td>

                            <td class="w-50 px-3 py text-center text-xxs ">
                                DESCRIPCION
                            </td>

                            <td class="w-10 px-3 py text-center text-xxs " >
                                CANTIDAD
                            </td>
                            <td class="w-10 px-10 py text-center text-xxs" >
                                PRECIO UNITARIO
                            </td>
                            <td class="w-10 px-10 py text-center text-xxs" >
                                SUBTOTAL
                            </td>
                        </tr>
                    </thead>

                    <tbody>
                        @php
                            $total_valor = 0;
                        @endphp
                        @foreach ($items as  $item)
                            <tr>
                                <td class="text-left text-xxs uppercase font-bold px-10 py-3"> {{$item->product_key}}</td>
                                <td class="text-left text-xxs uppercase font-bold px-10 py-3"> {{$item->note}}</td>
                                <td class="text-right text-xxs uppercase font-bold px-3 py-3"> {{Util::twoDecimals($item->cost)}}</td>
                                <td class="text-right text-xxs uppercase font-bold px-3 py-3"> {{Util::twoDecimals($item->quantity)}}</td>
                                <td class="text-right text-xxs uppercase font-bold px-3 py-3"> {{Util::twoDecimals($item->quantity*$item->cost)}}</td>
                            </tr>
                            @php
                                $total_valor +=(float) $item->quantity*$item->cost
                            @endphp

                        @endforeach
                            <tr>
                                <td colspan="4" class=" bg-grey-darker  text-white text-right text-xxs uppercase font-bold px-15 py-3"> Total Valor</td>
                                <td class="text-right text-xxs uppercase font-bold px-3 py-3"> {{Util::twoDecimals($total_valor)}}</td>
                            </tr>
                            <tr>
                                <td colspan="4" class=" bg-grey-darker   text-white text-right text-xxs uppercase font-bold px-15 py-3"> Descuento</td>
                                <td class="text-right text-xxs uppercase font-bold px-3 py-3"> {{ Util::twoDecimals($form->discount) }}</td>
                            </tr>
                            <tr>
                                <td colspan="4" class=" bg-grey-darker  text-white text-right text-xxs uppercase font-bold px-15 py-3"> Total a Pagar</td>
                                <td class="text-right text-xxs uppercase font-bold px-3 py-3"> {{Util::twoDecimals($total_valor-$form->discount)}}</td>
                            </tr>
                    </tbody>
                </table>

            </div>
            <br>
            <div class="block">
                <table class="table-info w-100">
                    <tbody>
                        <tr class="font-medium  ">
                            <td class=" text-white text-sm bg-grey-darker w-10 px-3 py text-center">
                                Son:
                            </td>
                            <td class=" text-xxs px-10" >
                                {{Util::convertirNumeroLetra(($total_valor-$form->discount)>0?($total_valor-$form->discount):0,'Bs')}}
                            </td>
                        </tr>
                    </tbody>

                </table>
            </div>
            <br>
            <footer class="text-right text-xxs">
               Creado por ARXAYIRI
            </footer>
            {{-- <div class="watermark">
                Vista Preliminar
            </div> --}}
        </div>
    </div>
</body>

</html>
