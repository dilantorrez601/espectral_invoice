<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use Auth;
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        // return 'hola';
        $sort_name= request('sort_name')??'first_name';
        $pagination_rows =request('pagination_rows')??10;
        $conditions=[];

        $products = Product::with('category')->where($conditions)
                        ->orderBy('created_at','desc')
                         ->paginate($pagination_rows);

        return response()->json(compact('products'));
    }

    public function list()
    {
        $products = Product::with('unit')->select('id','product_key','note','cost')->get();
        return response()->json(compact('products'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $message = "Producto Actualizado";
        $type_message = "info";

        if($request->id==0)
        {
            $message = "Producto Creado";
            $type_message = "success";
            $product = new Product;
        }else{

            $product = Product::find($request->id);
        }

        $product->account_id = Auth::user()->account_id;
        $product->user_id = Auth::user()->id;
        $product->product_key = $request->product_key;
        $product->note = $request->note;
        $product->cost = $request->cost;
        $product->category_id =$request->category['id'];
        $product->unit_id =$request->unit['id'];
        $product->save();

        return response()->json(compact('message','type_message'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
