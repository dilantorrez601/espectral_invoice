<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Unit;
use Auth;
class UnitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $sort_name= request('sort_name')??'first_name';
        $pagination_rows =request('pagination_rows')??10;
        $conditions=[];

        $units = Unit::where($conditions)
        ->orderBy('created_at','desc')
         ->paginate($pagination_rows);
        // return response()->json([
        //     'units' => $units->toArray(),
        //     'total'=>$total,
        // ]);

        return response()->json(compact('units'));
    }

    public function list(){
        $units = Unit::select('id','name')->get();
        return response()->json(compact('units'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $message = "Unidad de Medida Actualizada";
        $type_message = "info";

        if($request->id==0)
        {
            $message = "Unidad de Medida Creada";
            $type_message = "success";
            $unit = new Unit;
        }else{

            $unit = Unit::find($request->id);
        }

        $unit->account_id = Auth::user()->account_id;
        $unit->user_id = Auth::user()->id;
        $unit->name = $request->name;
        $unit->short_name = $request->short_name;
        // $unit->is_int = $request->is_int=='true'?true:false;
        $unit->is_int = $request->is_int;

        $unit->save();

        return response()->json(compact('message','type_message'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $unit = Unit::find($id);
        $unit->delete();
        $message = "Unidad Eliminada";
        $type_message = "info";
        return response()->json(compact('message','type_message'));
    }
}
