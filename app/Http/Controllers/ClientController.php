<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Client;
use App\Models\Contact;
use Auth;
class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        //$order = request('order')??'';
        $sort_name= request('sort_name')??'first_name';
        $pagination_rows =request('pagination_rows')??10;
        $conditions=[];

        $clients = Client::with('contacts')->where($conditions)
                        ->orderBy('created_at','desc')
                         ->paginate($pagination_rows);
        // return response()->json([
        //     'clients' => $clients->toArray(),
        //     'total'=>$total,
        // ]);

        return response()->json(compact('clients'));
    }

    public function list()
    {
        $clients = Client::with('contacts')->select('id','name','nit')->get();
        return response()->json(compact('clients'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //return response()->json(['asd'=>$request->name]);
        //return $request->all();
        $message = "Datos de Cliente Actualizados";
        $type_message = "info";

        if($request->id==0)
        {
            $message = "Cliente Registrado";
            $type_message = "success";
            $client = new Client;
        }else{
            $client = Client::find($request->id);
        }


        $client->account_id = Auth::user()->account_id;
        $client->user_id = Auth::user()->id;
        $client->name = $request->name;
        $client->nit = $request->nit;
        $client->business_name = $request->business_name;
        $client->address1 = $request->address1;
        $client->private_notes = $request->private_note;
        $client->state = $request->state;
        $client->work_phone = $request->work_phone;
        $client->city = $request->city;
        $client->save();

        $contacts = json_decode(json_encode($request->contacts));

        foreach($contacts as $contact)
        {
            if($contact->first_name!='')
            {
                $new_contact= new Contact;
                $new_contact->client_id = $client->id;
                $new_contact->account_id = $client->account_id;
                $new_contact->user_id = $client->user_id;
                $new_contact->first_name= $contact->first_name;
                $new_contact->last_name= $contact->last_name??null;
                $new_contact->phone= $contact->phone??null;
                $new_contact->email= $contact->email??null;
                $new_contact->position= $contact->position??null;
                $new_contact->save();
            }
        }


        return response()->json(compact('message','type_message','contacts'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
