<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Auth;
use App\Models\Branch;
use App\Models\DocumentType;
class ReportController extends Controller
{
    //

    public function invoicePdf(Request $request)
    {
        // $title = "Ingreso de Madera Procesada (Predimensionada)";
        // $date = date('d-m-Y');
        // $username = Auth::user()->username;
        // $subtitle = "Del " . $from . " al " . $to;
        // $hasStorage =false;
        // $storage="";
        // $view = \View::make('reports.lumber_predimensionado', compact('title','storage','hasStorage', 'subtitle', 'date', 'username', 'to', 'from', 'package_lumbers', 'total', 'type_report'));
        // $master_document= $request->master_document_id;

        $document_type = DocumentType::where('account_id',Auth::user()->account_id)
                                    ->where('master_document_id',$request->master_document_id)
                                    ->first();
        $branch = Branch::find(1);
        $account = Auth::user()->account;
        $user = Auth::user();
        $form = json_decode(json_encode($request->all()));
        $items = json_decode($request->jitems);
        // $client = $request->client;
        $client = json_decode( $request->client);
        $title = "Nota de Entrega";
        // $document_type = DocumentType::find(1);
        $view = \View::make('layouts.invoice',compact('document_type','user','branch','form','title','items','client'));
        $html_content = $view->render();
        $pdf = App::make('snappy.pdf.wrapper');
        $pdf->loadHTML($html_content);

        return $pdf->download('historial.pdf');
    }
}
