<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Invoice;
use App\Models\InvoiceItem;
use App\Models\DocumentType;
use App\Models\Branch;
use App\Models\Unit;
use App\Models\Product;
use Auth;
class NoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $sort_name= request('sort_name')??'first_name';
        $pagination_rows =request('pagination_rows')??10;
        $conditions=[];
        $master_document_id = 1 ; //obtener de la vista este deberia estar como constante XD
        $document_type= DocumentType::where('account_id',Auth::user()->account_id)->where('master_document_id',$master_document_id)->first();; //obetener de las variables en axios XD aunque sabiendo
        $notes = Invoice::with('client','invoice_status','invoice_items')->where($conditions)
                        ->where('document_type_id',$document_type->id)
                        ->orderBy('created_at','desc')
                         ->paginate($pagination_rows);

        return response()->json(compact('notes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $message = "Nota Actualizada";
        $type_message = "info";

        if($request->id==0)
        {
            $message = "Nota Creada";
            $type_message = "success";
            $invoice = new Invoice;
        }
        else{
            $invoice = Invoice::find($request->id);
        }
        $master_document_id = 1 ; //obtener de la vista XD
        $branch =Branch::find(1);

        $client = json_decode($request->client);
        $document_type = DocumentType::where('account_id',Auth::user()->account_id)->where('master_document_id',$master_document_id)->first();
        $invoice->account_id = Auth::user()->account_id;
        $invoice->user_id = Auth::user()->id;
        $invoice->client_id = $client->id;
        $invoice->document_type_id = $document_type->id;
        $invoice->invoice_status_id = 1;//revisar en base 1  emitido
        $invoice->invoice_number = $request->invoice_number;
        $invoice->invoice_date = $request->date;
        $invoice->discount = $request->discount;
        //datos cliente
        $invoice->client_name = $client->name;
        //datos de la cuenta
        $invoice->account_name = Auth::user()->account->name;
        $invoice->account_nit = Auth::user()->account->nit;
        //datos de la sucursal
        $invoice->branch_id = $branch->id; //deberia venir de la base de datos
        $invoice->branch_name = $branch->name; //deberia venir de la base de datos
        $invoice->address1 = $branch->address1; //deberia venir de la base de datos
        $invoice->phone = $branch->work_phone; //deberia venir de la base de datos
        $invoice->address1 = $branch->address1; //deberia venir de la base de datos
        $invoice->save();

        $items = json_decode($request->jitems);
        foreach ($items as $key => $item) {
            # code...
            if($type_message="success") //solo para saber si es nueva la nota caso contrario crear algoritmo de actualizacion de items XD
            {

                $product  = Product::find($item->id);
                $invoice_item = new InvoiceItem;
                $invoice_item->account_id = $invoice->account_id;
                $invoice_item->invoice_id = $invoice->id;
                $invoice_item->user_id = $invoice->user_id;
                $invoice_item->branch_id = $invoice->branch_id;
                $invoice_item->product_id = $item->id;
                $invoice_item->product_key = $item->product_key;
                $invoice_item->notes = $item->note;
                $invoice_item->cost = $item->cost;
                $invoice_item->quantity = $item->quantity;
                $invoice_item->discount = 0; //no hay descuento a un producto especifico hay que eliminar o ver que funcionalidad se le da
                //informacion adcional
                $invoice_item->is_product = true;  //deberia jalar de la unidad hdp
                $invoice_item->unit = $product->unit->name;
                $invoice_item->save();

            }
        }

        return response()->json(compact('message','type_message'));
        // master_document_id: this.master_document_id,
        // invoice_number: this.invoice_number,
        // date: this.date,
        // client: JSON.stringify(this.client),
        // discount: this.amount_discount,
        // jitems : JSON.stringify(this.product_list)

        // $invoice->

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
